// common function 
function ajax1(url, containerid, id)
{
	var page_request = false;
	if (window.XMLHttpRequest) // if Mozilla, Safari etc
		page_request = new XMLHttpRequest()
	else if (window.ActiveXObject){ // if IE
		try {
			page_request = new ActiveXObject("Msxml2.XMLHTTP")
		} 
		catch (e){
			try{
				page_request = new ActiveXObject("Microsoft.XMLHTTP")
			}
			catch (e){}
			}
		}
	else
		return false
	page_request.onreadystatechange=function()
	{
	ajaxResponse1(page_request, containerid, id)
	}
	page_request.open('GET', url, true)
	page_request.send(null)
}

function ajaxResponse1(page_request, containerid, id)
{
	if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1))
	{
		if (page_request.responseText != "")
		{
			document.getElementById(id).value = "";
			document.getElementById(id).focus();
		}
		document.getElementById(containerid).innerHTML=page_request.responseText;
		setTimeout('hideLoadingDiv()',600);
	}
}



function showLoadingDiv(w,h,mssg)
{
	var wd=300,hi=140,msg='Please Wait....';
	if(w) wd = w;	
	if(h)	hi = h;	
	if(mssg) msg = mssg;
	
	var main_div = document.getElementById('main_loading_div');
	var inner_div = document.getElementById('inner_loading_div');
	var loading_msg_div = document.getElementById('loading_msg');
	
	loading_msg_div.innerHTML = msg;
	
	var total_w = 0;
	var total_h = 0;
	//var inner_left = (total_w/2)- wd;
	var inner_left = (total_w/2);
	
	// OUTER DIV STYLE
	main_div.style.top = '50';
	main_div.style.left = '100';
	main_div.style.width = total_w+'px';
	main_div.style.Height = total_h+'px';
	
	// INNER DIV STYLE
	
	//inner_div.style.left = inner_left+'px';
	inner_div.style.top = '200px';
	inner_div.style.width = wd+'px';
	inner_div.style.height = hi+'px';
	
	document.getElementById('main_loading_div').style.display = 'block';
	document.getElementById('inner_loading_div').style.display = 'block';
}

function hideLoadingDiv(){
	document.getElementById('main_loading_div').style.display = 'none';
	document.getElementById('inner_loading_div').style.display = 'none';
}
