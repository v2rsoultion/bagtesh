<?php 
	require ("config/connection.php");
	require ("config/common_functions.php");
	require ("config/mysql.php");
	include("include/functions.php");
	include ("config/database_tables.php");
?>
<script src="<?php echo $SITEPATH2?>js/jquery-1.11.0.min.js" language="javascript" type="text/javascript"></script>
<style>
.tablemeasuerment
{
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
}
.tablemeasuerment input[type=text]
{
border-radius:7px;
height: 26px;
border:1px solid #ccc;
width:130px;
padding-left:5px;
}
.tablemeasuerment span
{
color:#FF0000;
}
.desc1
{
line-height:20px;
padding-top:10px;
text-align:justify;
padding-bottom:20px;
}
.messurment
{
font-family:Arial, Helvetica, sans-serif;
font-size:13px;
}
.slideimg{
float:left; 
width:57%;
}
.slideimg1{
float:left; 
width:40%;
}
.smallpop
{
padding:5px 10px 6px 10px;
border-radius:4px;
margin-left:4px;
color:#FFFFFF;
float:left;
text-align:center;
background:#ff6600;
}
.savedata
{
margin-top:20px;
}
.savedata a
{
width:100px;
text-align:center;
padding:0px 0px 0px 0px;
border:solid 1px;
color:#FFFFFF;
background:#ff6600;
font-family:Arial, Helvetica, sans-serif;
font-weight:bold;
padding: 7px 16px 7px 16px;
}
#resdiv
{
padding-left:5px;
color:#ff6600;
}
.sizechart1
{
text-transform: uppercase;
background: #ff821d;
text-align: center;
padding: 4px 6px 4px 6px;
margin-left: 4px;
border: 1px solid #ff6600;
cursor: pointer;
font-family:Arial, Helvetica, sans-serif;
float: left;
color: #fff;
transition: all .3s ease-in-out;
-webkit-transition: all .3s ease-in-out;
border-radius: 2px;
}
.slidediv
{
width:200px;
position:absolute;
top:0px;
right:52px;
margin:3px;
color: #000000;
background: #ff821d; 
border:solid 1px #000000; 
font-family:Arial, Helvetica, sans-serif; 
font-size:11px;
min-height:35px;
border-radius:3px;
padding:10px 5px 10px 5px; 
}
.showdata
{
position:relative;
float:left; 
width:27px; 
height:22px; 
border-radius:4px; 
text-align:center;
padding-top:5px; 
color:#FFFFFF; 
background:#ff821d;	
}
.others
{
float:left;
border-radius: 7px;
height: 70px;
border: 1px solid #ccc;
width: 130px;
padding-left: 5px;resize:none;
}
.slidediv
{
display:none;
}
.showdata:hover .slidediv
{
display:block;
}
.headr
{
font-size:16px;
margin-top:10px;
margin-bottom:10px;
}
</style>
<script>

function savedata()
{	
	jQuery(document).ready(function ($) {
		$("#form_step_1").submit(function (e) {
			var formObj = $(this);
			var formURL = formObj.attr("action");
			if( window.FormData !== undefined) {
				var formData = new FormData(this);
				$.ajax({
					url: formURL,
					type: "POST",
					data:  formData,
					//mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
					success: function( data, textStatus, jqXHR ) {
						$('#resdiv').html(data);
						//parent.$.fancybox.close();
					},
					error: function( jqXHR, textStatus, errorThrown ) {
						alert('not done');							
					}
				});
				e.preventDefault();
			}
		});
	});
	
	
}
      function isNumberKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}

function validate()
{
		if(document.getElementById("titletype1").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype1").focus();
			return false;
		}
		else if(document.getElementById("titletype2").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype2").focus();
			return false;
		}
		else	if(document.getElementById("titletype3").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype3").focus();
			return false;
		}
	else	if(document.getElementById("titletype4").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype4").focus();
			return false;
		}
	else	if(document.getElementById("titletype5").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype5").focus();
			return false;
		}
	else	if(document.getElementById("titletype6").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype6").focus();
			return false;
		}
		else	if(document.getElementById("titletype7").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype7").focus();
			return false;
		}
	else	if(document.getElementById("titletype8").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype8").focus();
			return false;
		}
	else if(document.getElementById("titletype9").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype9").focus();
			return false;
		}
	else if(document.getElementById("titletype10").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype10").focus();
			return false;
		}
	else	if(document.getElementById("titletype11").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype11").focus();
			return false;
		}
	else  if(document.getElementById("titletype12").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype12").focus();
			return false;
		}
	else	if(document.getElementById("titletypea1").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypea1").focus();
			return false;
		}
	else if(document.getElementById("titletypea2").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypea2").focus();
			return false;
		}
	else if(document.getElementById("titletypea3").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypea3").focus();
			return false;
		}
	else	if(document.getElementById("titletypea4").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypea4").focus();
			return false;
		}
	else  if(document.getElementById("titletypea5").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypea5").focus();
			return false;
		}
	else	if(document.getElementById("titletypeb1").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb1").focus();
			return false;
		}
	else if(document.getElementById("titletypeb2").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb2").focus();
			return false;
		}
	else if(document.getElementById("titletypeb3").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb3").focus();
			return false;
		}
	else	if(document.getElementById("titletypeb4").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb4").focus();
			return false;
		}
	else  if(document.getElementById("titletypeb5").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb5").focus();
			return false;
		}
	else if(document.getElementById("titletypeb6").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb6").focus();
			return false;
		}
	else	if(document.getElementById("titletypeb7").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb7").focus();
			return false;
		}
	else  if(document.getElementById("titletypeb8").value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletypeb8").focus();
			return false;
		}
		else  
		{
			alert("All Measuerment  Fill Value");
			return false;
		}

}
function savedata1()
{
parent.$.fancybox.close();
}
</script>
<?php
        $qry=mysql_query("SELECT * FROM  ".TABLE_SALWAR.""); 
	$fetch11=	mysql_fetch_array($qry);
 ?>
	<div style="text-align:right; font-size:10px; color:#ff6600; font-family:Arial, Helvetica, sans-serif">For Close Click on It </div>
<div class="messurment">

<div><h2 style="color:#ff6600"><?php echo $fetch11['salwartitle'] ?></h2></div>

<div class="desc1"><?php echo $fetch11['discription'] ?></div>
<div>
	<div class="slideimg"><img src="salwar_img/<?php echo $fetch11['sal_image'] ?>" width="100%" /></div>
	<div class="slideimg1">
		<form name="form_step_1" onsubmit="return validate();" action="<?php echo $SITEPATH2 ?>bagtesh_ajax.php?actiondata=addmeasuerment&pid=<?php echo $_GET['pid']?>&mtype=<?php echo $_REQUEST['Type'] ?>&Measuerment=<?php echo $_REQUEST['Measuerment'] ?>&other=<?php echo $_REQUEST['other'] ?>" id="form_step_1">
				<input type="hidden" name="Name" value="<?php echo $fetch11['salwartitle'] ?>" />
		<?php /*?><input type="hidden" name="mes_name" value="<?php echo $_GET['custom_mes_id']?>" /><?php */?>

		<div><h4 style="color:#ff6600; text-align:center;">Measuerment In Inch<input type="radio" name="Type" value="Inch" checked="checked"  />or Centimeters <input type="radio" name="Type"    value="Centimeter" /></h4></div>
<!-- TOP Measuerment-->

<div class="headr">Measurements for Kameez (Top)
<input type="hidden" name="Measurements for Kameez (Top)"  />
</div>
		<div>
		<div style="float:left; width:94%">
	
			<table class="tablemeasuerment">
			<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['title']);
			$t=0;
			foreach($detail  as $fetchdet) {
			$counter_mes++;
			$t++;
			?>
				<tr height="22px;">
					<td><?php echo $t."." ?>&nbsp;&nbsp;<?php echo $fetchdet ?>
					<span>*</span></td>
					<td>
						<input type="text"    name="<?php echo $fetchdet?>" id="titletype<?php echo $counter_mes?>" onkeypress="return isNumberKey(event)" style="float:left" />
						</td>
				</tr>
			<?php
			}
			?>
			</table>
			</div>
		<div style="float:left;width:5%">
				<table>
			
							<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['description']);
			foreach($detail  as $fetchdet) {
			?>
	<tr>
								<td>
			<div  class="showdata">?
			
			<div  class="slidediv">
			<?php echo $fetchdet?>
			</div>
			</div>

			
					</td>
			<?php }?>
				</tr>
				</table>
					</div>
					
		<div style=" clear:both;"></div>
		</div>
<!-- TOP Measuerment-->
<div>
<input type="radio" name="Bottom_Style"  value="As Shown" />&nbsp;As Shown&nbsp;
<input type="radio" name="Bottom_Style"  value="Salwar" />&nbsp;Salwar&nbsp;<br />
<input type="radio" name="Bottom_Style"  value="Chudidar" />&nbsp;Chudidar&nbsp;&nbsp&nbsp;
<input type="radio" name="Bottom_Style"  value="Patiala" />&nbsp&nbsp;Patiala&nbsp;
</div>
<!-- MIddel Measuerment-->

<div class="headr">Measurements for Salwar (Bottom)
<input type="hidden" name="Measurements for Salwar (Bottom)" value=""  /></div>


		<div>
		<div style="float:left; width:94%">
	
			<table class="tablemeasuerment">
			<?php
			$counter_mes1 = 0;
			$detail=json_decode($fetch11['title2']);
			$t1=0;
			foreach($detail  as $fetchdet) {
			$counter_mes1++;
			$t1++;
			?>
				<tr height="22px;">
					<td><?php echo $t1."." ?>&nbsp;&nbsp;<?php echo $fetchdet ?>
					<span>*</span></td>
					<td>
						<input type="text"    name="<?php echo $fetchdet?>" id="titletypea<?php echo $counter_mes1?>" onkeypress="return isNumberKey(event)" style="float:left" />
						</td>
		
				</tr>
			<?php
			}
			?>
					
				
			

			</table>
			</div>
		<div style="float:left;width:5%">
				<table>
			
							<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['desc2']);
			$t1=0;
			foreach($detail  as $fetchdet) {
			$counter_mes1++;
			$t1++;
			?>
	<tr>
								<td>
			<div  class="showdata">?
			
			<div  class="slidediv">
			<?php echo $fetchdet?>
			</div>
			</div>
					</td>
			<?php }?>
				</tr>
				</table>
					</div>
					
		<div style=" clear:both;"></div>
		</div>
<!-- Middle Measuerment-->
<!-- Bottom Measuerment-->

<div class="headr">Measurements for Churidar (Bottom)
<input type="hidden" name="Measurements for Churidar (Bottom)" value="" />
</div>


		<div>
		<div style="float:left; width:94%">
	
			<table class="tablemeasuerment">
			<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['title3']);
			$t2=0;
			foreach($detail  as $fetchdet) {
			$counter_mes2++;
			$t2++;
			?>
				<tr height="22px;">
					<td><?php echo $t2."." ?>&nbsp;&nbsp;<?php echo $fetchdet ?>
					<span>*</span></td>
					<td>
						<input type="text"    name="<?php echo $fetchdet?>" id="titletypeb<?php echo $counter_mes2?>" onkeypress="return isNumberKey(event)" style="float:left" />
						</td>
		
				</tr>
			<?php
			}
			?>
					
				
			
	<tr height="22px;">
					<td>&nbsp;&nbsp;Other
				</td>
					<td>
				<textarea name="other" id="other" class="others"></textarea>
						</td>
				</tr>
			</table>
			</div>
		<div style="float:left;width:5%">
				<table>
				<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['desc3']);
			$t=0;
			foreach($detail  as $fetchdet) {
			$counter_mes2++;
			$t2++;
			?>
	<tr>
								<td>
			<div  class="showdata">?
			
			<div  class="slidediv">
			<?php echo $fetchdet?>
			</div>
			</div>

			
					</td>
			<?php }?>
				</tr>
				</table>
					</div>
					
		<div style=" clear:both;"></div>
		</div>
<!-- Bottom Measuerment-->
	



<div style=" clear:both;"></div>

			<div id="resdiv" style="width:100%;"></div>
			<div class="savedata">
				<input type="submit" name="cus_mes" id="sux_mes" value="Save" class="sizechart1" onclick="savedata()" />
				<input type="button" name="cus_mes1" id="sux_mes1" value="Cancle" class="sizechart1" onclick="savedata1()" />
			</div>
		</form>
		
	
	</div>
</div>

<div class="clear"></div>

</div>