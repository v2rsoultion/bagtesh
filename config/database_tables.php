<?php

define('TABLE_ADMIN', 'admin');

define('TABLE_CATEGORY', 'category');

define('TABLE_PRODUCT', 'product');

define('TABLE_MASTER_PAGES','master_pages');

define('TABLE_CONTACTUS', 'contact');

define('TABLE_SOCIAL', 'social_icons');

define('TABLE_ENQUIRY', 'enquiry');

define('TABLE_NEWS', 'news_letter');

define('TABLE_CLIENT_VIEW', 'client_view');

define('TABLE_SLIDER', 'slider');

define('TABLE_PRODUCT_SIZE_PRICE', 'productpricesize');

define('TABLE_OCCASION', 'occasion');

define('TABLE_FABRIC', 'fabric');

define('TABLE_COLOR', 'color');

define('TABLE_COUPON', 'coupon');

define('TABLE_SIZE', 'prod_size');

define('TABLE_MESURE', 'measurement');

define('TABLE_MESURE_DETAIL', 'measurement_detail');

define('TABLE_USER', 'user_details');

define('TABLE_COUNTRY', 'country');

define('TABLE_FAQ', 'faq');

define('TABLE_SHIPPING', 'shpping');

define('TABLE_SIDEIMG', 'side_image');

define('TABLE_WISHLIST', 'wishlist');

define('TABLE_GALLARY', 'gallary');

define('TABLE_BLOUSEDESIGN', 'blouse_desgin');

define('TABLE_CHECKOUT', 'checkout');

define('TABLE_CART', 'cart');

define('TABLE_SAREE', 'saree_mes');

define('TABLE_ACCESS', 'accessories');

define('TABLE_ENQ', 'product_enquery');

define('TABLE_EMAILTRACK', 'email_track');

define('TABLE_RECIPROCALLINK', 'rciprocal');

define('TABLE_SALWAR', 'salwar_mes');


?>