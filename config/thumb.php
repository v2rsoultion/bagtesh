<?php
/*
  Tye Shavik (tye at tye . ca)
*/

//create_thumbnail($_FILES["txtUpload1"]['tmp_name'],$filepath3,125,94);

error_reporting(0);
function ConvertBMP2GD($src, $dest = false) {
        if(!($src_f = fopen($src, "rb"))) {
                return false;
        }
        if(!($dest_f = fopen($dest, "wb"))) {
                return false;
        }
        $header = unpack("vtype/Vsize/v2reserved/Voffset", fread($src_f,
14));
        $info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant",
fread($src_f, 40));

        extract($info);
        extract($header);

        if($type != 0x4D42) {           // signature "BM"
                return false;
        }

        $palette_size = $offset - 54;
        $ncolor = $palette_size / 4;
        $gd_header = "";
        // true-color vs. palette
        $gd_header .= ($palette_size == 0) ? "\xFF\xFE" : "\xFF\xFF";      
        $gd_header .= pack("n2", $width, $height);
        $gd_header .= ($palette_size == 0) ? "\x01" : "\x00";
        if($palette_size) {
                $gd_header .= pack("n", $ncolor);
        }
        // no transparency
        $gd_header .= "\xFF\xFF\xFF\xFF";                                    

        fwrite($dest_f, $gd_header);

        if($palette_size) {
                $palette = fread($src_f, $palette_size);
                $gd_palette = "";
                $j = 0;
                while($j < $palette_size) {
                        $b = $palette{$j++};
                        $g = $palette{$j++};
                        $r = $palette{$j++};
                        $a = $palette{$j++};
                        $gd_palette .= "$r$g$b$a";
                }
                $gd_palette .= str_repeat("\x00\x00\x00\x00", 256 - $ncolor);
                fwrite($dest_f, $gd_palette);
        }

        $scan_line_size = (($bits * $width) + 7) >> 3;
        $scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size &
0x03) : 0;

        for($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {
                // BMP stores scan lines starting from bottom
                fseek($src_f, $offset + (($scan_line_size + $scan_line_align) *
$l));
                $scan_line = fread($src_f, $scan_line_size);
                if($bits == 24) {
                        $gd_scan_line = "";
                        $j = 0;
                        while($j < $scan_line_size) {
                                $b = $scan_line{$j++};
                                $g = $scan_line{$j++};
                                $r = $scan_line{$j++};
                                $gd_scan_line .= "\x00$r$g$b";
                        }
                }
                else if($bits == 8) {
                        $gd_scan_line = $scan_line;
                }
                else if($bits == 4) {
                        $gd_scan_line = "";
                        $j = 0;
                        while($j < $scan_line_size) {
                                $byte = ord($scan_line{$j++});
                                $p1 = chr($byte >> 4);
                                $p2 = chr($byte & 0x0F);
                                $gd_scan_line .= "$p1$p2";
                        }
                        $gd_scan_line = substr($gd_scan_line, 0, $width);
                }
                else if($bits == 1) {
                        $gd_scan_line = "";
                        $j = 0;
                        while($j < $scan_line_size) {
                                $byte = ord($scan_line{$j++});
                                $p1 = chr((int) (($byte & 0x80) != 0));
                                $p2 = chr((int) (($byte & 0x40) != 0));
                                $p3 = chr((int) (($byte & 0x20) != 0));
                                $p4 = chr((int) (($byte & 0x10) != 0));
                                $p5 = chr((int) (($byte & 0x08) != 0));
                                $p6 = chr((int) (($byte & 0x04) != 0));
                                $p7 = chr((int) (($byte & 0x02) != 0));
                                $p8 = chr((int) (($byte & 0x01) != 0));
                                $gd_scan_line .= "$p1$p2$p3$p4$p5$p6$p7$p8";
                        }
                        $gd_scan_line = substr($gd_scan_line, 0, $width);
                }

                fwrite($dest_f, $gd_scan_line);
        }
        fclose($src_f);
        fclose($dest_f);
        return true;

}

function imagecreatefrombmp($filename) {
        $tmp_name = tempnam("/tmp", "GD");
        if(ConvertBMP2GD($filename, $tmp_name)) {
                $img = imagecreatefromgd($tmp_name);
                unlink($tmp_name);
                return $img;
        }
        return false;

}
function create_thumbnail($infile,$outfile,$maxw,$maxh,$stretch = FALSE) {
  clearstatcache();
  if (!is_file($infile)) {
    trigger_error("Cannot open file: $infile",E_USER_WARNING);
    return FALSE;
  }
  if (is_file($outfile)) {
      trigger_error("Output file doesnt exists: $outfile",E_USER_WARNING);
    return FALSE;
  }

  $functions = array(
    'image/png' => 'ImageCreateFromPng',
    'image/jpeg' => 'ImageCreateFromJpeg',
	'image/bmp' => 'ImageCreateFromBmp',
  );

  // Add GIF support if GD was compiled with it
  if (function_exists('ImageCreateFromGif')) { $functions['image/gif'] = 'ImageCreateFromGif'; }

  $size = getimagesize($infile);

  // Check if mime type is listed above
  if (!$function = $functions[$size['mime']]) {
      trigger_error("MIME T�r unsupported: {$size['mime']}",E_USER_WARNING);
    return FALSE;
  }

  // Open source image
  if (!$source_img = $function($infile)) {
      trigger_error("Unable Kime open source file: $infile",E_USER_WARNING);
    return FALSE;
  }

  $save_function = "image" . strtolower(substr(strrchr($size['mime'],'/'),1));

  // Scale dimensions
  list($neww,$newh) = scale_dimensions($size[0],$size[1],$maxw,$maxh,$stretch);

  if ($size['mime'] == 'image/png') {
    // Check if this PNG image is indexed
    $temp_img = imagecreatefrompng($infile);
    if (imagecolorstotal($temp_img) != 0) {
      // This is an indexed PNG
      $indexed_png = TRUE;
    } else {
      $indexed_png = FALSE;
    }
    imagedestroy($temp_img);
  } 
  if ($size['mime'] == 'image/bmp') {
    // Check if this PNG image is indexed
    $temp_img = imagecreatefrombmp($infile);
    if (imagecolorstotal($temp_img) != 0) {
      // This is an indexed PNG
      $indexed_png = TRUE;
    } else {
      $indexed_png = FALSE;
    }
    imagedestroy($temp_img);
  }  

  // Create new image resource
  if ($size['mime'] == 'image/gif' || ($size['mime'] == 'image/png' && $indexed_png)) {
    // Create indexed 
    $new_img = imagecreate($neww,$newh);

    // Copy the palette
    imagepalettecopy($new_img,$source_img);
    
    $color_transparent = imagecolortransparent($source_img);
    if ($color_transparent >= 0) {
      // Copy transparency
      imagefill($new_img,0,0,$color_transparent);
      imagecolortransparent($new_img, $color_transparent);
    }
  } else {
    $new_img = imagecreatetruecolor($neww,$newh);
  }
  
  // Copy and resize image
  imagecopyresampled($new_img,$source_img,0,0,0,0,$neww,$newh,$size[0],$size[1]);

  // Save output file
  if ($save_function == 'imagejpeg') {
      // Change the JPEG quality here
      if (!$save_function($new_img,$outfile,100)) {
          trigger_error("Unable Kime Kaydet output image",E_USER_WARNING);
          return FALSE;
      }
  } else { 
  		if($save_function=='imagebmp')
			$save_function='imagepng';
      if (!$save_function($new_img,$outfile)) {
          trigger_error("Unable Kime Kaydet output image",E_USER_WARNING);
          return FALSE;
      }
  }
  // Cleanup
  imagedestroy($source_img);
  imagedestroy($new_img);

  return TRUE;
}
// Scales dimensions
function scale_dimensions($w,$h,$maxw,$maxh,$stretch = FALSE) {
    if (!$maxw && $maxh) {
      // Width is unlimited, scale by width
      $newh = $maxh;
      if ($h < $maxh && !$stretch) { $newh = $h; }
      else { $newh = $maxh; }
      $neww = ($w * $newh / $h);
    } elseif (!$maxh && $maxw) {
      // Scale by height
      if ($w < $maxw && !$stretch) { $neww = $w; }
      else { $neww = $maxw; }
      $newh = ($h * $neww / $w);
    } elseif (!$maxw && !$maxh) {
      return array($w,$h);
    } else {
      if ($w / $maxw > $h / $maxh) {
        // Scale by height
        if ($w < $maxw && !$stretch) { $neww = $w; }
        else { $neww = $maxw; }
        $newh = ($h * $neww / $w);
      } elseif ($w / $maxw <= $h / $maxh) {
        // Scale by width
        if ($h < $maxh && !$stretch) { $newh = $h; }
        else { $newh = $maxh; }
        $neww = ($w * $newh / $h);
      }
    }
    return array(round($neww),round($newh));
}
?>