<?php 
$errors = '';
$visitor_email = '';
$user_message = '';
$selectcont = mysql_fetch_array( mysql_query("select * from ".TABLE_CONTACTUS));
if(isset($_POST['save']) && $_POST['cn_name']!="")
{
	if(empty($_SESSION['6_letters_code']) || strcasecmp($_SESSION['6_letters_code'], $_POST['6_letters_code']) != 0)
	{
		$errors .= "\n Sorry !! The captcha code does not match!";
	}
	else
	{
		$name=mysql_real_escape_string(trim(strip_tags($_POST['cn_name'])));
		$visitor_email=mysql_real_escape_string(trim(strip_tags($_POST['cn_email'])));
		$phone=mysql_real_escape_string(trim(strip_tags($_POST['cn_phone'])));
		$message=mysql_real_escape_string(trim(strip_tags($_POST['cn_msg'])));
		///------------Do Validations-------------
		if(empty($name)||empty($visitor_email))
		{
			$errors .= "\n Name and Email are required fields. ";	
		}
		if(IsInjected($visitor_email))
		{
			$errors .= "\n Bad email value!";
		}
		
		if(empty($errors))
		{
		$ct_to=$selectcont['email_sales'];
    $ct_subject = "This e-mail was sent from a Contact from of Bagtesh Fashion";
 $ct_message = '
      <html>
      <head>
        <title>'.$ct_subject.'</title>
      </head>
      <body>
       <table cellpadding="0" cellspacing="0" border="0" width="70%" align="left">
        <tr>
          <td height="15"></td>
        </tr>
        <tr>  
          <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" >
			<tr>
                <td><b>Message : </b><br>'.$message.' ;  </td>
              </tr>
              <tr>
                <td colspan="2" height="8">&nbsp;</td>
              </tr>
			<tr>
			
        		  
			<tr>
                <td><b>Name : </b><br>'.$name.' ;  </td>
              </tr>
              <tr>
                <td colspan="2" height="8">&nbsp;</td>
              </tr>
			<tr>
                <td><b>Phone Number : </b><br>'.$phone.' ;  </td>
              </tr>
              <tr>
                <td colspan="2" height="8">&nbsp;</td>
              </tr>
			  <tr>
                <td><b>From : </b><br>'.$visitor_email.' ;  </td>
              </tr>
              <tr>
                <td colspan="2" height="8">&nbsp;</td>
              </tr>

			   <tr>
                <td><b>--</b><br>'.$SITEPATH2.'</td>
              </tr>
              <tr>
                <td colspan="2" height="8">&nbsp;</td>
              </tr>
			  
                </tr>
         
            </table>
          </td>
         </tr>             
      </table>
      </body>
      </html> ';
	      
    // To send HTML mail, the Content-type header must be set
    $ct_headers  = 'MIME-Version: 1.0' . "\r\n";
    $ct_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
			$ct_headers .= 'To: '.SITE_TITLE.' <'.$ct_to.'>' . "\r\n";
			$ct_headers .= 'From: '.$_REQUEST['cn_email'].' <'.$_REQUEST['cn_email'].'>' . "\r\n";
				
    // Additional headers
	  if(mail( $ct_to, $ct_subject, $ct_message, $ct_headers ))
	  {
	  	echo '<script> window.location ="thank-you?msg=con" </script>';
	  }
	  
 	}
	
	}

}
function IsInjected($str)
{
  $injections = array('(\n+)',
			  '(\r+)',
			  '(\t+)',
			  '(%0A+)',
			  '(%0D+)',
			  '(%08+)',
			  '(%09+)'
			  );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
   if(preg_match($inject,$str))
	{
	return true;
  }
  else
	{
	return false;
  }
}
?>
<script type="text/javascript">
function refreshCaptcha() {
	var img = document.images['captch1'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}

function validate_contactus()
	{
		
		re = /^[A-Za-z]+$/;
		
		te = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
		se = /^[0-9A-Za-z ]+[A-Za-z ]+$/;
		if(document.getElementById('cn_name').value=="")
			{
				alert('Please Enter Your Name');
				document.getElementById('cn_name').focus();
				return false;
			}			
		if(!re.test(document.getElementById('cn_name').value))
		{
				alert('Please Enter Your Correct name');
				document.getElementById('cn_name').focus();
				return false;
			}
		
		
		var email = document.getElementById('cn_email');
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if ((email==null)||(email=="")){
			alert("Please Enter Your Mail Id");
			email.focus();
			return false;
		}
		else
		if (!filter.test(email.value)) {
		alert('Please Enter Correct Mail Id');
		document.getElementById('cn_email').focus();
		return false;
		}
		
		
		if(document.getElementById('cn_email').value=="")
			{
				alert('Please Enter Your email');
				document.getElementById('cn_email').focus();
				return false;
			}
		if(document.getElementById('cn_phone').value=="")
			{
				alert('Please Enter Your Phone Number');
				document.getElementById('cn_phone').focus();
				return false;
			}

			if(document.getElementById('cn_msg').value=="")
			{
				alert('Please Enter Your Message');
				document.getElementById('cn_msg').focus();
				return false;
			}
					if(document.getElementById('6_letters_code').value=="")
			{
				alert('Please Enter The Captch Code');
				document.getElementById('6_letters_code').focus();
				return false;
			}
	}
</script>
<div class="middle_part_outer">
  <div class="middle_part">
    <h1>Contact Us</h1>
	<div class="conmid">
    <div class="contactform1">
     <p>Fill out this form and we'll get back to you.</p>
      <form method="post" action="" onSubmit="return validate_contactus();">
        <ul>
          <li>
            <div class="controw"><i class="fa fa-user"></i><input id="cn_name" name="cn_name" type="text" placeholder="Name" value="<?php echo $_REQUEST['cn_name']?>"/></div>
          </li>
          <li>
            <div class="controw"><i class="fa fa-envelope"></i><input id="cn_email" name="cn_email" type="text" placeholder="E-mail" value="<?php echo $_REQUEST['cn_email']?>"/></div>
          </li>
          <li>
            <div class="controw"><i class="fa fa-phone"></i><input id="cn_phone" name="cn_phone" type="text" placeholder="Enter Phone Number" value="<?php echo $_REQUEST['cn_phone']?>"/></div>
          </li>
          <li>
            <div class="conttextarea"><i class="fa fa-comment" style="float:left;"></i><textarea id="cn_msg" name="cn_msg" placeholder="What's Your Question?"><?php echo $_REQUEST['cn_msg']?></textarea></div>
          </li>
          <div class="captch">
            <div style="float:left"><img src="captcha/captcha_code_file.php?rand=<?php echo rand(); ?>"  id="captch1"/></div>
            <div style="float:left">
              <input type="text" name="6_letters_code" value="" class="txtbox_contact_capch" id='6_letters_code'/>
            </div>
            <div class="clear"></div>
            <small>Can't read the image? click <a href='javascript: refreshCaptcha();' style="color:#FF0000">here</a> to refresh</small> </div>
          <div class="error" align="left">
            <?php if(!empty($errors)){echo "<p>".nl2br($errors)."</p>";}?>
          </div>
          <li>
            <input type="submit" name="save" value="Contact Us"/>
          </li>
        </ul>
      </form>
    </div>
    <div class="contcadd">
      <?php 
	$sel_contact=mysql_query("select * from ".TABLE_CONTACTUS." where contactus_id=1");
	$fetch_contact=mysql_fetch_array($sel_contact);
	?>
      <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;&nbsp; <?php echo nl2br($fetch_contact['contactus_address']);?></p>
      <p><i class="fa fa-phone"></i> &nbsp;&nbsp;&nbsp;<?php echo $fetch_contact['contactus_mobile1'];?></p>
      <p><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;<a href="mailto:<?php echo $fetch_contact['contactus_email1'];?>"><?php echo $fetch_contact['contactus_email1'];?></a></p>
    </div>
    <div class="clear"></div>
    <div class="contactterm">
    <p>You may contact our customer care for anything pertaining to Bagtesh Fashion. Our customer care support help customer for:</p>
    <p><i class="fa fa-arrow-circle-right"></i> Order Related Queries</p>
    <p><i class="fa fa-arrow-circle-right"></i> Password Related Queries</p>
    <p><i class="fa fa-arrow-circle-right"></i> Delivery Related Queries</p>
    <p><i class="fa fa-arrow-circle-right"></i> Product Related Queries</p>
    <p><i class="fa fa-arrow-circle-right"></i> Login Related Queries</p>
    <p><i class="fa fa-arrow-circle-right"></i> Payment Related Queries</p>
     <p><i class="fa fa-arrow-circle-right"></i> Anything that the customer may clarify about Bagtesh Fashion</p>
    <p>We have created FAQ's with our answer, you can also find those queries in the FAQ section.</p>
    </div>
    <div class="gmap">
	<iframe src="<?php echo $fetch_contact['contactus_map'];?>" width="1100" height="450" frameborder="0" style="border:0"></iframe></div>
    <div class="clear"></div>
        <?php if($pages[0]=="error") {?> 
    <div style="color:#f00;">
    All fields are required...
    </div>

    <?php }?>      
      </div>
	</div>
</div>
