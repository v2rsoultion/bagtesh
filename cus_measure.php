<?php
	require ("config/connection.php");
	require ("config/common_functions.php");
	require ("config/mysql.php");
	include("include/functions.php");
	include ("config/database_tables.php");
?>

<script src="<?php echo $SITEPATH2?>js/jquery-1.11.0.min.js" language="javascript" type="text/javascript"></script>
<style>
.tablemeasuerment
{
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
}
.tablemeasuerment input[type=text]
{
border-radius:7px;
height: 26px;
border:1px solid #ccc;
width:130px;
padding-left:5px;
}
.tablemeasuerment span
{
color:#FF0000;
}
.desc1
{
line-height:20px;
padding-top:10px;
text-align:justify;
padding-bottom:20px;
}
.messurment
{
font-family:Arial, Helvetica, sans-serif;
font-size:13px;
}
.slideimg{
float:left;
width:57%;
}
.slideimg1{
float:left;
width:40%;
}
.smallpop
{
padding:5px 10px 6px 10px;
border-radius:4px;
margin-left:4px;
color:#FFFFFF;
float:left;
text-align:center;
background:#ff6600;
}
.savedata
{
margin-top:20px;
}
.savedata a
{
width:100px;
text-align:center;
padding:0px 0px 0px 0px;
border:solid 1px;
color:#FFFFFF;
background:#ff6600;
font-family:Arial, Helvetica, sans-serif;
font-weight:bold;
padding: 7px 16px 7px 16px;
}
#resdiv
{
padding-left:5px;
color:#ff6600;
}
.sizechart1
{
text-transform: uppercase;
background: #ff821d;
text-align: center;
padding: 4px 6px 4px 6px;
margin-left: 4px;
border: 1px solid #ff6600;
cursor: pointer;
font-family:Arial, Helvetica, sans-serif;
float: left;
color: #fff;
transition: all .3s ease-in-out;
-webkit-transition: all .3s ease-in-out;
border-radius: 2px;
}
.slidediv
{
width:200px;
position:absolute;
top:0px;
right:52px;
margin:3px;
color: #000000;
background: #ff821d;
border:solid 1px #000000;
font-family:Arial, Helvetica, sans-serif;
font-size:11px;
min-height:35px;
border-radius:3px;
padding:10px 5px 10px 5px;
}
.showdata
{
position:relative;
float:left;
width:27px;
height:22px;
border-radius:4px;
text-align:center;
padding-top:5px;
color:#FFFFFF;
background:#ff821d;
}
.others
{
float:left;
border-radius: 7px;
height: 70px;
border: 1px solid #ccc;
width: 130px;
padding-left: 5px;resize:none;
}
.slidediv
{
display:none;
}
.showdata:hover .slidediv
{
display:block;
}
.savedata111{
	width: 40px;
margin: 0px auto;
}
</style>
<script>

function savedata()
{
	jQuery(document).ready(function ($) {
		$("#form_step_1").submit(function (e) {
			var formObj = $(this);
			var formURL = formObj.attr("action");
			if( window.FormData !== undefined) {
				var formData = new FormData(this);
				$.ajax({
					url: formURL,
					type: "POST",
					data:  formData,
					//mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
					success: function( data, textStatus, jqXHR ) {
						$('#resdiv').html(data);
						//parent.$.fancybox.close();
					},
					error: function( jqXHR, textStatus, errorThrown ) {
						//alert('not done');
					}
				});
				e.preventDefault();
			}
		});
	});
}
      function isNumberKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}

function validate()
{
	var tot=document.getElementById("tot").value;
	for(i=1;i<=tot;i++)
	{
		if(document.getElementById("titletype"+i).value=="")
		{
			alert("Please Fill Value");
			document.getElementById("titletype"+i).focus();
			return false;
		}
	}
	for(i=1;i<=tot;i++)
	{
	if(document.getElementById("titletype"+i).value!="")
		{
			$("#allmes_part").hide();
			$("#thankdiv").show();
			return false;
		}
	}
}
function savedata1()
{
parent.$.fancybox.close();
}
</script>
	<div style="text-align:right; font-size:10px; color:#ff6600; font-family:Arial, Helvetica, sans-serif">For Close Click on It </div>
	<div id="thankdiv" style="margin-top:10px; display:none" >
	<div  style="width:50% ; margin:0px auto;">
	<h3>
	Your measurement has successfully submitted.
	</h3>
	</div>

		<div class="savedata111" >
				<input type="button" name="cus_mes1" id="sux_mes1" value="Ok" class="sizechart1" onclick="savedata1()" />
			</div>
		</div>

<div class="messurment" id="allmes_part">
<?php
$query="SELECT * FROM `measurement` where m_id='".$_GET['custom_mes_id']."'";
$fetch11=mysql_fetch_array(mysql_query($query));
?>
<div><h2 style="color:#ff6600"><?php echo $fetch11['title'] ?></h2></div>


<div class="desc1"><?php echo $fetch11['discription'] ?></div>
<div>
	<div class="slideimg"><img src="custom_size/<?php echo $fetch11['m_chart'] ?>" width="100%" /></div>
	<div class="slideimg1">
		<form name="form_step_1" onsubmit="return validate();" action="<?php echo $SITEPATH2 ?>bagtesh_ajax.php?actiondata=addmeasuerment&pid=<?php echo $_GET['pid']?>&mtype=<?php echo $_REQUEST['Type'] ?>&Measuerment=<?php echo $_REQUEST['Measuerment'] ?>&other=<?php echo $_REQUEST['other'] ?>" id="form_step_1">
				<input type="hidden" name="Name" value="<?php echo $fetch11['title'] ?>" />
		<?php /*?><input type="hidden" name="mes_name" value="<?php echo $_GET['custom_mes_id']?>" /><?php */?>
		<input type="hidden" name="mes_name" value="<?php echo $_GET['custom_mes_id']?>" />

		<div><h4 style="color:#ff6600; text-align:center;">Measurement In Inch<input type="radio" name="Type" value="Inch" checked="checked"  />or Centimeters <input type="radio" name="Type"    value="Centimeter" /></h4></div>

		<div style="float:left; width:94%">
			<table class="tablemeasuerment">
			<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['measuerment_title']);
			$t=0;
			foreach($detail  as $fetchdet) {
			$counter_mes++;
			$t++;
			?>
				<tr height="22px;">
					<td><?php echo $t."."; ?>&nbsp;&nbsp;<?php echo $fetchdet ?>
					<span>*</span></td>
					<td>
						<input type="text"    name="<?php echo $fetchdet?>" id="titletype<?php echo $counter_mes?>" onkeypress="return isNumberKey(event)" style="float:left" />
						</td>

				</tr>
			<?php
			}
			?>
					<tr height="22px;">
					<td>&nbsp;&nbsp;Other
				</td>
					<td>
				<textarea name="other" id="other" class="others"></textarea>
						</td>
				</tr>
			</table>
			</div>
				<div style="float:left;width:5%">
				<table>

							<?php
			$counter_mes = 0;
			$detail=json_decode($fetch11['measuerment_detail']);
			$t=0;
			foreach($detail  as $fetchdet) {
			$counter_mes++;
			$t++;
			?>
	<tr>
								<td>
			<div  class="showdata">?
			<div  class="slidediv">
			<?php echo $fetchdet?>
			</div>
			</div>
					</td>
			<?php }?>
				</tr>
				</table>
					</div>
					<div style=" clear:both;"></div>
			<div id="resdiv" style="width:100%;"></div>
			<div class="savedata">
				<input type="submit" name="cus_mes" id="sux_mes" value="Save" class="sizechart1" onclick="savedata()" />
				<input type="button" name="cus_mes1" id="sux_mes1" value="Cancle" class="sizechart1" onclick="savedata1()" />
			</div>
		</form>
		<input type="hidden" name="tot" id="tot" value="<?php  echo $t;?>" />
	</div>
</div>
<div class="clear"></div>
</div>
