-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 12:54 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v2rteste_bagtesh`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessories`
--

CREATE TABLE `accessories` (
  `accessory_id` bigint(20) UNSIGNED NOT NULL,
  `accessory_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `accessory_price` double(8,2) NOT NULL,
  `accessory_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `account_setting`
--

CREATE TABLE `account_setting` (
  `account_setting_id` bigint(20) UNSIGNED NOT NULL,
  `site_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `site_logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `site_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `site_address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `profile_image` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `name`, `email`, `mobile_number`, `email_verified_at`, `profile_image`, `password`, `user_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bagtesh Fashion', 'info@bagtesh.com', NULL, NULL, NULL, '$2y$10$qchOWPArId/6wI1lKIdtwecCtFCr0ieBqLDQQy.V/JTgfhHS2lLrO', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `api_tokens`
--

CREATE TABLE `api_tokens` (
  `api_token_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` bigint(20) UNSIGNED NOT NULL,
  `banner_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `banner_description` text COLLATE utf8mb4_unicode_ci,
  `banner_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Product, 2:Category',
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `banner_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blouse_designs`
--

CREATE TABLE `blouse_designs` (
  `blouse_design_id` bigint(20) UNSIGNED NOT NULL,
  `blouse_design_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Also used as Alt text',
  `blouse_design_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blouse_design_order` int(11) NOT NULL DEFAULT '0',
  `blouse_design_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active,  0  = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blouse_designs`
--

INSERT INTO `blouse_designs` (`blouse_design_id`, `blouse_design_name`, `blouse_design_image`, `blouse_design_order`, `blouse_design_status`, `created_at`, `updated_at`) VALUES
(2, 'testing1', '971e3ba2-1f94-494c-97fc-0155c404581e-1581752647.jpg', 2, 1, '2020-02-15 07:44:07', '2020-02-20 19:06:58'),
(5, 'testing for id', 'a8677dd5-9e9a-4948-9c04-c10af47ed974-1582180672.jpg', 2, 1, '2020-02-20 18:07:52', '2020-02-20 19:06:58'),
(6, 'testing new', 'd2fa2a74-e750-4418-97c9-c883164a3c9a-1582180763.jpg', 2, 1, '2020-02-20 18:09:23', '2020-02-20 19:06:58'),
(7, 'testingqwerty', 'fec40cfc-02eb-4bc2-92b4-4826136fb6f1-1582181027.jpg', 0, 0, '2020-02-20 18:13:47', '2020-02-20 19:06:58'),
(8, '1', 'f512a798-72ee-4fd0-af40-02bb70387783-1582182224.jpg', 0, 1, '2020-02-20 18:33:44', '2020-02-20 19:06:58'),
(9, '12', '705433aa-ba61-4e52-bca2-3486f92d8047-1582182492.jpg', 0, 0, '2020-02-20 18:38:12', '2020-02-20 19:06:58'),
(10, '123', 'ace16b0c-d0f4-426a-b50d-5d8bb56617af-1582183072.png', 25, 1, '2020-02-20 18:47:53', '2020-02-20 19:06:58'),
(11, 'zxcvasdqwe', '8f1ba036-9000-4dcf-904d-2c98487cc737-1582199211.jpg', 23, 1, '2020-02-20 23:16:51', '2020-02-20 23:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `bottom_salwar_measurements`
--

CREATE TABLE `bottom_salwar_measurements` (
  `bottom_id` bigint(20) UNSIGNED NOT NULL,
  `bottom_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bottom_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bottom_salwar_measurements`
--

INSERT INTO `bottom_salwar_measurements` (`bottom_id`, `bottom_title`, `bottom_description`, `created_at`, `updated_at`) VALUES
(3, 'testing1', 'testing2', '2020-02-14 07:43:39', '2020-02-14 07:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `category_order` int(11) NOT NULL DEFAULT '0',
  `category_root_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_image` text COLLATE utf8mb4_unicode_ci,
  `size_chart` text COLLATE utf8mb4_unicode_ci,
  `category_desc` text COLLATE utf8mb4_unicode_ci,
  `category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_subroot_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `category_meta_title` text COLLATE utf8mb4_unicode_ci,
  `category_meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `category_meta_description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_order`, `category_root_id`, `category_name`, `category_image`, `size_chart`, `category_desc`, `category_status`, `created_at`, `updated_at`, `category_subroot_id`, `category_meta_title`, `category_meta_keywords`, `category_meta_description`) VALUES
(16, 2, 15, 'testing child', 'e96690d4-b416-4063-be8d-36b5ffda6260-1581157026.jpg', '5f62cf8a-3399-4108-8a2b-c6c3062319c0-1581157026.jpg', '<p>NONE&nbsp; &nbsp; &nbsp;</p>', 0, '2020-02-08 10:17:06', '2020-02-20 19:07:18', 0, '<p>NONE</p>', '<p>NONE</p>', '<p>NONE</p>'),
(18, 2, 0, 'testing', '25befa78-7da5-44b2-8bda-087016837d1f-1581336343.jpg', 'None', '<p>qwer</p>', 0, '2020-02-10 12:05:43', '2020-02-20 19:07:18', 0, NULL, NULL, NULL),
(19, 2, 0, 'testingcat', '0a0db93f-d4fb-414e-8419-c171e4ab8448-1581512102.jpg', 'dea9a2d3-5297-468a-b5a3-0e5ee27292d9-1581512102.jpg', NULL, 0, '2020-02-12 12:55:02', '2020-02-20 19:07:18', 0, NULL, NULL, NULL),
(20, 1, 19, 'testing cat', '3bda5f79-966a-4461-a255-7a52e72fe387-1581512180.jpg', '9eb2f4e8-ef15-42b0-8278-1d89a93cfebe-1581512180.jpg', NULL, 0, '2020-02-12 12:56:20', '2020-02-20 19:07:18', 16, NULL, NULL, NULL),
(21, 2, 18, 'testing for live', '2ce15277-ce3d-463f-8029-1c5a9d70a906-1582200837.jpg', '520415d5-a971-435c-adf7-677f570b5a21-1582200837.jpg', NULL, 1, '2020-02-20 23:43:57', '2020-02-20 23:43:57', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `color_id` bigint(20) UNSIGNED NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `color_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`color_id`, `ordering`, `color_name`, `color_code`, `created_at`, `updated_at`, `color_status`) VALUES
(3, 0, 'Red', '#fa1313', '2020-02-07 12:03:40', '2020-02-12 07:12:17', 1),
(4, 0, 'black', '#000000', '2020-02-20 23:34:18', '2020-02-20 23:34:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `country_name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactice',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_status`, `created_at`, `updated_at`) VALUES
(5, 'testing', 1, '2020-02-17 10:34:54', '2020-02-18 10:06:46'),
(6, 'testing1', 1, '2020-02-17 11:34:43', '2020-02-17 11:34:43'),
(7, 'a', 1, '2020-02-18 11:00:19', '2020-02-18 11:00:19'),
(8, 'b', 1, '2020-02-18 11:00:27', '2020-02-18 11:00:27'),
(9, 'c', 1, NULL, NULL),
(10, 'd', 1, NULL, NULL),
(11, 'e', 1, NULL, NULL),
(12, 'f', 1, NULL, NULL),
(13, 'g', 1, NULL, NULL),
(14, 'india', 1, '2020-02-20 23:43:07', '2020-02-20 23:43:07');

-- --------------------------------------------------------

--
-- Table structure for table `coupans`
--

CREATE TABLE `coupans` (
  `coupan_id` bigint(20) UNSIGNED NOT NULL,
  `coupan_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL,
  `start_price` double(8,2) NOT NULL,
  `end_price` double(8,2) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `coupan_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupans`
--

INSERT INTO `coupans` (`coupan_id`, `coupan_code`, `discount`, `start_price`, `end_price`, `start_date`, `end_date`, `created_at`, `updated_at`, `coupan_status`) VALUES
(1, 'testing1', 3.40, 23.00, 213.00, '2020-02-25', '2020-02-27', '2020-02-18 12:51:10', '2020-02-20 05:47:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `enquiry_id` bigint(20) UNSIGNED NOT NULL,
  `enquiry_item_code` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry_user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enquiry_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry_phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry_status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 = Pending , 2 = Replyed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `febrics`
--

CREATE TABLE `febrics` (
  `febric_id` bigint(20) UNSIGNED NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `febric_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `febric_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT ' 1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `febrics`
--

INSERT INTO `febrics` (`febric_id`, `ordering`, `febric_name`, `febric_status`, `created_at`, `updated_at`) VALUES
(2, 2, 'testing', 0, '2020-02-10 12:02:28', '2020-02-10 12:05:08'),
(3, 2, 'testing1', 1, '2020-02-10 12:04:40', '2020-02-10 12:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE `gallary` (
  `gallary_id` bigint(20) UNSIGNED NOT NULL,
  `gallary_title` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Used As Alt text also',
  `gallary_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gallary_desc` text COLLATE utf8mb4_unicode_ci,
  `gallary_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = Inactive, 1 = Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_pages`
--

CREATE TABLE `master_pages` (
  `master_page_id` bigint(20) UNSIGNED NOT NULL,
  `page_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_name_slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_sort_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_long_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `measurements`
--

CREATE TABLE `measurements` (
  `measurement_id` bigint(20) UNSIGNED NOT NULL,
  `measurement_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `measurement_price` double(8,2) NOT NULL,
  `measurement_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `measurement_chart` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `measurement_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `detail_title` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `measurements`
--

INSERT INTO `measurements` (`measurement_id`, `measurement_title`, `measurement_price`, `measurement_desc`, `measurement_chart`, `measurement_status`, `created_at`, `updated_at`, `detail_title`) VALUES
(8, 'testing', 2.50, '<p>qwertty&nbsp;&nbsp;&nbsp;&nbsp;</p>', 'b5d89d1c-6623-49dc-b8b9-82e323bfb65d-1581317354.jpg', 0, '2020-02-10 06:49:14', '2020-02-12 07:20:17', NULL),
(9, 'testing new', 2.00, '<p>qwerty</p>', '0583b0bf-3366-484c-b980-cf2b8b19fa67-1581317605.jpg', 1, '2020-02-10 06:53:25', '2020-02-12 07:36:28', NULL),
(10, 'testing measure', 23.00, '<p>123</p>', '6fcb68ff-88c5-4657-98a7-06c95bbc618f-1581512240.jpg', 1, '2020-02-12 12:57:20', '2020-02-12 12:57:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `measurement_details`
--

CREATE TABLE `measurement_details` (
  `measurement_detail_id` bigint(20) UNSIGNED NOT NULL,
  `measurement_id` bigint(20) UNSIGNED NOT NULL,
  `measurement_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `measurement_details`
--

INSERT INTO `measurement_details` (`measurement_detail_id`, `measurement_id`, `measurement_title`, `title_description`, `details_status`, `created_at`, `updated_at`) VALUES
(1, 9, 'testing 1', 'testing 1 data', 1, '2020-02-10 06:53:25', '2020-02-10 06:53:25'),
(2, 9, 'testing 2', 'testing 2 data', 1, '2020-02-10 06:53:25', '2020-02-10 06:53:25'),
(3, 9, 'testing 3', 'testing 3 data', 1, '2020-02-10 06:53:26', '2020-02-10 06:53:26'),
(4, 10, 'qwreqwe', 'qweqwe', 1, '2020-02-12 12:57:20', '2020-02-12 12:57:20'),
(5, 10, 'qweqwe', 'qweqwe', 1, '2020-02-12 12:57:20', '2020-02-12 12:57:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_05_08_064810_create_categories_table', 1),
(9, '2019_05_08_082133_create_products_table', 1),
(10, '2019_05_08_085540_create_product_images_table', 1),
(11, '2019_05_08_090909_create_product_carts_table', 1),
(12, '2019_05_08_092126_create_product_orders_table', 1),
(13, '2019_05_08_095502_create_user_whish_lists_table', 1),
(14, '2019_05_08_100313_create_banners_table', 1),
(15, '2019_05_08_114418_create_product_order_details_table', 1),
(16, '2019_05_09_045111_create_testimonials_table', 1),
(17, '2019_05_09_045803_create_master_pages_table', 1),
(18, '2019_05_10_110907_create_user_addresses_table', 1),
(19, '2019_05_11_062121_create_api_tokens_table', 1),
(20, '2019_05_13_070828_create_user_devices_table', 1),
(21, '2019_05_14_094450_create_admin_users_table', 1),
(22, '2019_05_31_095049_createaccountsetting', 1),
(23, '2019_06_04_045725_add_field_banner', 1),
(24, '2019_06_10_090752_create_notification', 1),
(25, '2019_06_10_104444_add_field_user', 1),
(26, '2019_06_22_062838_add_field_category', 1),
(27, '2019_06_22_070540_add_field_order_details', 1),
(28, '2020_01_22_035141_add_category_desc_to_categories', 1),
(29, '2020_01_23_125338_create_color_table', 1),
(30, '2020_01_24_075728_add_color_status_to_colors', 1),
(31, '2020_01_24_103833_add_category_subroot_id_to_category', 1),
(32, '2020_01_24_111446_add_size_chart_to_categories', 1),
(33, '2020_01_25_061758_add_columns_to_categories', 1),
(34, '2020_01_27_101434_create_febrics_table', 1),
(35, '2020_01_27_105925_add_febric_status_to_febrics', 2),
(36, '2020_01_27_111205_add_category_order_to_categories', 2),
(37, '2020_01_27_111234_add_category_order_to_colors', 2),
(38, '2020_01_27_111250_add_category_order_to_febrics', 2),
(39, '2020_01_28_111212_create_occasion_table', 2),
(40, '2020_01_28_122405_create_sizes_table', 2),
(41, '2020_01_29_073435_create_accessories_table', 2),
(42, '2020_01_29_104401_create_measurements_table', 2),
(43, '2020_01_30_075624_add_cols_to_measurements', 2),
(44, '2020_01_30_103031_create_measurement_details_table', 2),
(45, '2020_01_31_101433_create_pages_table', 2),
(46, '2020_01_31_101518_create_blouse_designs_table', 2),
(47, '2020_01_31_101657_create_orders_table', 2),
(48, '2020_01_31_101746_create_gallary_table', 2),
(49, '2020_01_31_101831_create_shipping_charges_table', 3),
(50, '2020_01_31_102008_create_saree_measurements_table', 3),
(51, '2020_01_31_102024_create_salwar_measurements_table', 3),
(52, '2020_01_31_102042_create_slider_images_table', 3),
(53, '2020_01_31_102144_create_enquiries_table', 3),
(54, '2020_01_31_102227_create_countries_table', 3),
(55, '2020_01_31_102321_create_social_urls_table', 3),
(56, '2020_01_31_102404_create_news_letters_table', 3),
(57, '2020_02_13_131842_create_top_salwar_measurements_table', 4),
(58, '2020_02_13_132031_create_bottom_salwar_measurements_table', 4),
(59, '2020_02_13_135238_add_salwar_description_to_salwar_measurements', 5),
(62, '2020_02_18_163548_create_coupans_table', 6),
(63, '2020_02_18_182408_add_coupan_status_to_coupans', 7);

-- --------------------------------------------------------

--
-- Table structure for table `news_letters`
--

CREATE TABLE `news_letters` (
  `news_letter_id` bigint(20) UNSIGNED NOT NULL,
  `news_letter_email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_letter_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `product_id` bigint(20) NOT NULL DEFAULT '0',
  `product_order_id` bigint(20) NOT NULL DEFAULT '0',
  `notification_type` bigint(20) NOT NULL COMMENT '1:Top Deals, 2:Order Placed, 3:Order Completed, 4:Order Cancelled, 5:Order Failed, 6:Order Return',
  `notification_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `occasions`
--

CREATE TABLE `occasions` (
  `occasion_id` bigint(20) UNSIGNED NOT NULL,
  `occasion_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `occasion_order` int(11) NOT NULL DEFAULT '0',
  `occasion_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Actice , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `occasions`
--

INSERT INTO `occasions` (`occasion_id`, `occasion_name`, `occasion_order`, `occasion_status`, `created_at`, `updated_at`) VALUES
(2, 'testing', 0, 1, '2020-02-13 12:13:56', '2020-02-13 12:13:56'),
(3, 'testing11', 0, 1, '2020-02-17 11:00:40', '2020-02-17 11:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `order_user_id` bigint(20) UNSIGNED NOT NULL,
  `order_product_id` bigint(20) UNSIGNED NOT NULL,
  `order_quantity` tinyint(4) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1 = Progress , 2 = Shipped , 3 = Completed',
  `ordered_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `page_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_top_desc` text COLLATE utf8mb4_unicode_ci,
  `page_image` text COLLATE utf8mb4_unicode_ci,
  `page_middle_desc` text COLLATE utf8mb4_unicode_ci,
  `page_meta_title` text COLLATE utf8mb4_unicode_ci,
  `page_meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `page_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `product_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `product_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` decimal(8,2) NOT NULL,
  `product_discount` decimal(8,2) NOT NULL,
  `net_price` decimal(8,2) NOT NULL,
  `product_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `out_of_stock` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Yes, 2:No',
  `in_top_deal` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:Yes, 2:No',
  `is_best_selling` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:Yes, 2:No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_carts`
--

CREATE TABLE `product_carts` (
  `product_cart_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'for loggedin user',
  `product_quantity` int(11) NOT NULL DEFAULT '1',
  `user_device_id` text COLLATE utf8mb4_unicode_ci COMMENT 'for guest user',
  `cart_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_order` int(11) NOT NULL,
  `image_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `default_image` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:Yes, 2:No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `product_order_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_date` datetime NOT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `total_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `product_quantity` int(11) NOT NULL DEFAULT '0',
  `discount_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `net_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `shipping_charge` decimal(8,2) NOT NULL DEFAULT '0.00',
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `user_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `shipping_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_mode` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:COD, 2:Other',
  `order_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Success, 2:Cancelled, 3:Failed, 4: Return',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_order_details`
--

CREATE TABLE `product_order_details` (
  `product_order_detail_id` bigint(20) UNSIGNED NOT NULL,
  `product_order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variant_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(11) NOT NULL DEFAULT '1',
  `product_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hsl_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `discount_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `net_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `product_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `out_of_stock` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Yes, 2:No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salwar_measurements`
--

CREATE TABLE `salwar_measurements` (
  `salwar_measurement_id` bigint(20) UNSIGNED NOT NULL,
  `salwar_measurement_titles` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salwar_measurement_price` double(8,2) DEFAULT NULL,
  `salwar_measurement_chart` text COLLATE utf8mb4_unicode_ci,
  `salwar_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salwar_measurements`
--

INSERT INTO `salwar_measurements` (`salwar_measurement_id`, `salwar_measurement_titles`, `salwar_measurement_price`, `salwar_measurement_chart`, `salwar_description`, `created_at`, `updated_at`) VALUES
(2, 'testing 1', 1234.00, 'a0493899-79b4-4f74-8a9d-a4708c8243a5-1581591023.jpg', '<p>qwerrt</p>', '2020-02-13 10:50:23', '2020-02-13 10:50:23');

-- --------------------------------------------------------

--
-- Table structure for table `saree_measurements`
--

CREATE TABLE `saree_measurements` (
  `saree_measurement_id` bigint(20) UNSIGNED NOT NULL,
  `saree_measurement_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saree_measurement_price` double(8,2) NOT NULL,
  `saree_custom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `saree_measurement_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `saree_measurements`
--

INSERT INTO `saree_measurements` (`saree_measurement_id`, `saree_measurement_title`, `saree_measurement_price`, `saree_custom_id`, `saree_measurement_status`, `created_at`, `updated_at`) VALUES
(3, 'testing 1', 12.00, 9, 1, '2020-02-12 08:06:33', '2020-02-12 08:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_charges`
--

CREATE TABLE `shipping_charges` (
  `shipping_id` bigint(20) UNSIGNED NOT NULL,
  `shipping_country_id` bigint(20) UNSIGNED NOT NULL,
  `shipping_weight` double(8,2) NOT NULL,
  `shipping_price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_charges`
--

INSERT INTO `shipping_charges` (`shipping_id`, `shipping_country_id`, `shipping_weight`, `shipping_price`, `created_at`, `updated_at`) VALUES
(1, 6, 20.50, 2.20, '2020-02-18 10:06:01', '2020-02-18 10:06:01'),
(2, 6, 30.60, 8.60, '2020-02-18 10:06:01', '2020-02-18 10:06:01'),
(3, 5, 22.00, 12.00, '2020-02-18 10:07:08', '2020-02-18 10:07:08'),
(4, 5, 12.00, 0.00, '2020-02-18 10:07:08', '2020-02-18 10:07:31'),
(5, 5, 32.00, 21.00, '2020-02-18 10:08:37', '2020-02-18 10:08:37'),
(6, 5, 10.00, 1.00, '2020-02-18 10:08:58', '2020-02-18 10:08:58'),
(7, 6, 231.00, 32321.00, '2020-02-18 10:58:47', '2020-02-18 10:58:47');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `size_id` bigint(20) UNSIGNED NOT NULL,
  `size_measure` int(11) NOT NULL,
  `size_order` int(11) NOT NULL DEFAULT '0',
  `price_percent` double(8,2) NOT NULL DEFAULT '0.00',
  `size_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`size_id`, `size_measure`, `size_order`, `price_percent`, `size_status`, `created_at`, `updated_at`) VALUES
(2, 32, 3, 0.50, 1, '2020-02-14 10:14:01', '2020-02-14 10:14:30'),
(3, 31, 1, 0.50, 0, '2020-02-14 10:14:17', '2020-02-14 10:14:30');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `slider_id` bigint(20) UNSIGNED NOT NULL,
  `slider_title` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'aslo used as ALT text',
  `slider_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_link` text COLLATE utf8mb4_unicode_ci,
  `slider_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`slider_id`, `slider_title`, `slider_image`, `slider_link`, `slider_status`, `created_at`, `updated_at`) VALUES
(3, 'testing', '6606678f-05e1-4137-8fac-ff710b946054-1581766579.jpg', 'any link', 1, '2020-02-15 11:36:19', '2020-02-15 11:48:40');

-- --------------------------------------------------------

--
-- Table structure for table `social_urls`
--

CREATE TABLE `social_urls` (
  `social_url_id` bigint(20) UNSIGNED NOT NULL,
  `social_url_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_url_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_url_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active , 0 = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_urls`
--

INSERT INTO `social_urls` (`social_url_id`, `social_url_title`, `social_url_link`, `social_url_status`, `created_at`, `updated_at`) VALUES
(3, 'facebook', 'https://www.facebook.com/bagteshfashion/', 1, '2020-02-17 08:11:16', '2020-02-17 08:11:16'),
(4, 'testing2', 'testing2', 1, '2020-02-17 08:11:51', '2020-02-17 08:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review_message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `top_salwar_measurements`
--

CREATE TABLE `top_salwar_measurements` (
  `top_id` bigint(20) UNSIGNED NOT NULL,
  `top_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `top_salwar_measurements`
--

INSERT INTO `top_salwar_measurements` (`top_id`, `top_title`, `top_description`, `created_at`, `updated_at`) VALUES
(15, 'testing 1', 'TESTING 1', '2020-02-14 07:43:39', '2020-02-14 07:43:39'),
(17, 'trt', 'werew', '2020-02-14 07:44:26', '2020-02-14 07:44:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `profile_image` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_id` text COLLATE utf8mb4_unicode_ci,
  `user_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile_number`, `email_verified_at`, `profile_image`, `password`, `device_id`, `user_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bagtesh Fashion', 'info@bagtesh.com', NULL, NULL, NULL, '$2y$10$qchOWPArId/6wI1lKIdtwecCtFCr0ieBqLDQQy.V/JTgfhHS2lLrO', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `user_address_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `house_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `apartment_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `street_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_address` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Yes, 2:No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_devices`
--

CREATE TABLE `user_devices` (
  `user_device_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `device_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Yes, 2:No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_whish_lists`
--

CREATE TABLE `user_whish_lists` (
  `user_whish_list_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `whishlist_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active, 2:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessories`
--
ALTER TABLE `accessories`
  ADD PRIMARY KEY (`accessory_id`);

--
-- Indexes for table `account_setting`
--
ALTER TABLE `account_setting`
  ADD PRIMARY KEY (`account_setting_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_email_unique` (`email`);

--
-- Indexes for table `api_tokens`
--
ALTER TABLE `api_tokens`
  ADD PRIMARY KEY (`api_token_id`),
  ADD KEY `api_tokens_user_id_foreign` (`user_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`),
  ADD KEY `banners_category_id_foreign` (`category_id`);

--
-- Indexes for table `blouse_designs`
--
ALTER TABLE `blouse_designs`
  ADD PRIMARY KEY (`blouse_design_id`);

--
-- Indexes for table `bottom_salwar_measurements`
--
ALTER TABLE `bottom_salwar_measurements`
  ADD PRIMARY KEY (`bottom_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `coupans`
--
ALTER TABLE `coupans`
  ADD PRIMARY KEY (`coupan_id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`enquiry_id`);

--
-- Indexes for table `febrics`
--
ALTER TABLE `febrics`
  ADD PRIMARY KEY (`febric_id`);

--
-- Indexes for table `gallary`
--
ALTER TABLE `gallary`
  ADD PRIMARY KEY (`gallary_id`);

--
-- Indexes for table `master_pages`
--
ALTER TABLE `master_pages`
  ADD PRIMARY KEY (`master_page_id`);

--
-- Indexes for table `measurements`
--
ALTER TABLE `measurements`
  ADD PRIMARY KEY (`measurement_id`);

--
-- Indexes for table `measurement_details`
--
ALTER TABLE `measurement_details`
  ADD PRIMARY KEY (`measurement_detail_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_letters`
--
ALTER TABLE `news_letters`
  ADD PRIMARY KEY (`news_letter_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `occasions`
--
ALTER TABLE `occasions`
  ADD PRIMARY KEY (`occasion_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `orders_order_user_id_foreign` (`order_user_id`),
  ADD KEY `orders_order_product_id_foreign` (`order_product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_carts`
--
ALTER TABLE `product_carts`
  ADD PRIMARY KEY (`product_cart_id`),
  ADD KEY `product_carts_product_id_foreign` (`product_id`),
  ADD KEY `product_carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`product_order_id`),
  ADD KEY `product_orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_order_details`
--
ALTER TABLE `product_order_details`
  ADD PRIMARY KEY (`product_order_detail_id`),
  ADD KEY `product_order_details_product_order_id_foreign` (`product_order_id`),
  ADD KEY `product_order_details_product_id_foreign` (`product_id`);

--
-- Indexes for table `salwar_measurements`
--
ALTER TABLE `salwar_measurements`
  ADD PRIMARY KEY (`salwar_measurement_id`);

--
-- Indexes for table `saree_measurements`
--
ALTER TABLE `saree_measurements`
  ADD PRIMARY KEY (`saree_measurement_id`),
  ADD UNIQUE KEY `saree_measurements_saree_measurement_title_unique` (`saree_measurement_title`),
  ADD KEY `saree_measurements_saree_custom_id_foreign` (`saree_custom_id`);

--
-- Indexes for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `social_urls`
--
ALTER TABLE `social_urls`
  ADD PRIMARY KEY (`social_url_id`),
  ADD UNIQUE KEY `social_urls_social_url_title_unique` (`social_url_title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessories`
--
ALTER TABLE `accessories`
  MODIFY `accessory_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `account_setting`
--
ALTER TABLE `account_setting`
  MODIFY `account_setting_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blouse_designs`
--
ALTER TABLE `blouse_designs`
  MODIFY `blouse_design_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bottom_salwar_measurements`
--
ALTER TABLE `bottom_salwar_measurements`
  MODIFY `bottom_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `color_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `febrics`
--
ALTER TABLE `febrics`
  MODIFY `febric_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `measurements`
--
ALTER TABLE `measurements`
  MODIFY `measurement_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `measurement_details`
--
ALTER TABLE `measurement_details`
  MODIFY `measurement_detail_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `occasions`
--
ALTER TABLE `occasions`
  MODIFY `occasion_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salwar_measurements`
--
ALTER TABLE `salwar_measurements`
  MODIFY `salwar_measurement_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
