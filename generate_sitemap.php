<?php 

require ("config/connection.php");
require ("config/common_functions.php");
require ("config/mysql.php");
include ("include/functions.php");
include ("config/database_tables.php");

$site_url = $SITEPATH2;
$current_date = date('Y-m-d');
$daily = 'daily';
$weekly = 'weekly';

// Sitemap 1 for Home Page and Footer Links
$xml_001 = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
$xml_001 .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$daily.'</changefreq>'.
			'<priority>1.0</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'about-us</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'testimonials</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'faqs</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'contact-us</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'how-to-order</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'blouse-design</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'disclaimer</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'shipment-policy</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'return-policy</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'terms-of-use</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'privacy</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$weekly.'</changefreq>'.
			'<priority>0.7</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$xml_001 .= '</urlset>';
file_put_contents( "sitemap_001.xml", $xml_001 );

// Sitemap 2 for Cat & Sub Cat
$xml_002 = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
$xml_002 .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'.PHP_EOL;


$xml_002 .= '<url>'.PHP_EOL.
			'<loc>'.$site_url.'new</loc>'.
			'<lastmod>'.$current_date.'</lastmod>'.
			'<changefreq>'.$daily.'</changefreq>'.
			'<priority>0.9</priority>'.
			PHP_EOL.'</url>'.PHP_EOL;

$selcat = mysql_query("select * from ".TABLE_CATEGORY." where category_location=0 and category_status=1 order by category_id");
while($selcatfetch = mysql_fetch_array($selcat))
{

	$xml_002 .= '<url>'.PHP_EOL.
				'<loc>'.$site_url.$selcatfetch['category_slug'].'</loc>'.
				'<lastmod>'.$current_date.'</lastmod>'.
				'<changefreq>'.$daily.'</changefreq>'.
				'<priority>0.9</priority>'.
				PHP_EOL.'</url>'.PHP_EOL;

	$selsubcat= mysql_query("select * from ".TABLE_CATEGORY." where category_location=".$selcatfetch['category_id']." and category_status=1 order by s_order");
	while($selsub = mysql_fetch_array($selsubcat))
	{

		$xml_002 .= '<url>'.PHP_EOL.
					'<loc>'.$site_url.$selcatfetch['category_slug'].'/'.$selsub['category_slug'].'</loc>'.
					'<lastmod>'.$current_date.'</lastmod>'.
					'<changefreq>'.$daily.'</changefreq>'.
					'<priority>0.9</priority>'.
					PHP_EOL.'</url>'.PHP_EOL;

		$sesub= mysql_query("select * from ".TABLE_CATEGORY." where category_location=".$selsub['category_id']." and category_status=1 order by s_order");
		while($select_subin = mysql_fetch_array($sesub))
		{
			$xml_002 .= '<url>'.PHP_EOL.
						'<loc>'.$site_url.$selcatfetch['category_slug'].'/'.$selsub['category_slug'].'/'.$select_subin['category_slug'].'</loc>'.
						'<lastmod>'.$current_date.'</lastmod>'.
						'<changefreq>'.$daily.'</changefreq>'.
						'<priority>0.9</priority>'.
						PHP_EOL.'</url>'.PHP_EOL;
		}
	}
}


$xml_002 .= '</urlset>';
file_put_contents( "sitemap_002.xml", $xml_002 );

// Sitemap 3 for Products
$xml_003 = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
$xml_003 .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'.PHP_EOL;


$selcat = mysql_query("select * from ".TABLE_CATEGORY." where category_location=0 and category_status=1 order by category_id");
while($selcatfetch = mysql_fetch_array($selcat))
{

	$main_pro_query ="select * from ".TABLE_PRODUCT." where product_category='".$selcatfetch['category_id']."'";
	$main_fetch_pro = mysql_query($main_pro_query);
	
	if( $main_fetch_pro !='' ){
		while($main_pro_res = mysql_fetch_array($main_fetch_pro))
		{
				$xml_003 .= '<url>'.PHP_EOL.
					'<loc>'.$site_url.$selcatfetch['category_slug'].'/'.$main_pro_res['product_slug'].'</loc>'.
					'<image:image>'.
					'<image:loc>'.$site_url.'product_imagezoom/'.$main_pro_res['product_popimage'].'</image:loc>'.
					'<image:title>'.$main_pro_res['product_name'].'</image:title>'.
					'</image:image>'.
					'<lastmod>'.$current_date.'</lastmod>'.
					'<changefreq>'.$weekly.'</changefreq>'.
					'<priority>0.8</priority>'.
					PHP_EOL.'</url>'.PHP_EOL;
		}
	}

	$selsubcat= mysql_query("select * from ".TABLE_CATEGORY." where category_location=".$selcatfetch['category_id']." and category_status=1 order by s_order");
	while($selsub = mysql_fetch_array($selsubcat))
	{
		$sub_pro_query ="select * from ".TABLE_PRODUCT." where product_category='".$selsub['category_id']."'";
		$sub_fetch_pro = mysql_query($sub_pro_query);
		
		if( $sub_fetch_pro !='' ){
			while($sub_pro_res = mysql_fetch_array($sub_fetch_pro))
			{
					$xml_003 .= '<url>'.PHP_EOL.
						'<loc>'.$site_url.$selcatfetch['category_slug'].'/'.$selsub['category_slug'].'/'.$sub_pro_res['product_slug'].'</loc>'.
						'<image:image>'.
						'<image:loc>'.$site_url.'product_imagezoom/'.$sub_pro_res['product_popimage'].'</image:loc>'.
						'<image:title>'.$sub_pro_res['product_name'].'</image:title>'.
						'</image:image>'.
						'<lastmod>'.$current_date.'</lastmod>'.
						'<changefreq>'.$weekly.'</changefreq>'.
						'<priority>0.8</priority>'.
						PHP_EOL.'</url>'.PHP_EOL;
			}
		}

		$sesub= mysql_query("select * from ".TABLE_CATEGORY." where category_location=".$selsub['category_id']." and category_status=1 order by s_order");
		while($select_subin = mysql_fetch_array($sesub))
		{
			$pro_query ="select * from ".TABLE_PRODUCT." where product_category='".$select_subin['category_id']."'";
			$fetch_pro = mysql_query($pro_query);
			
			if( $fetch_pro !='' ){
				while($pro_res = mysql_fetch_array($fetch_pro))
				{
						$xml_003 .= '<url>'.PHP_EOL.
							'<loc>'.$site_url.$selcatfetch['category_slug'].'/'.$selsub['category_slug'].'/'.$select_subin['category_slug'].'/'.$pro_res['product_slug'].'</loc>'.
							'<image:image>'.
							'<image:loc>'.$site_url.'product_imagezoom/'.$pro_res['product_popimage'].'</image:loc>'.
							'<image:title>'.$pro_res['product_name'].'</image:title>'.
							'</image:image>'.
							'<lastmod>'.$current_date.'</lastmod>'.
							'<changefreq>'.$weekly.'</changefreq>'.
							'<priority>0.8</priority>'.
							PHP_EOL.'</url>'.PHP_EOL;
				}
			}
		}
	}
}

$xml_003 .= '</urlset>';
file_put_contents( "sitemap_003.xml", $xml_003 );

// Main Sitemap
$main_site_map = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;

$main_site_map .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.
		'<sitemap>'.
		'<loc>'.$site_url.'sitemap_001.xml'.'</loc>'.
		'<lastmod>'.$current_date.'</lastmod>'.
		'</sitemap>'.

		'<sitemap>'.
		'<loc>'.$site_url.'sitemap_002.xml'.'</loc>'.
		'<lastmod>'.$current_date.'</lastmod>'.
		'</sitemap>'.

		'<sitemap>'.
		'<loc>'.$site_url.'sitemap_003.xml'.'</loc>'.
		'<lastmod>'.$current_date.'</lastmod>'.
		'</sitemap>'.
		'</sitemapindex>';


file_put_contents( "sitemap.xml", $main_site_map );

?>