<div class="middle_part_inner">
  <div class="middle_part">
    <div class="loginform">
	  <div class="breadcump"><a href="<?php $SITEPATH2 ?>">Home</a> &raquo;My Account </div>
      <h1> My Account </h1>


      <div class="myaccountdiv">
        <div class="myaccounttop">
          <div class="myaccount">
            <?php
					switch ( $pages[0] ) {
						case 'change-password' 			: $currentclass  = 'myaccountlinkhover'; break;
						case 'edit-profile' 			: $currentclass1 = 'myaccountlinkhover'; break;
						case 'order-history-details' 	: $currentclass2 = 'myaccountlinkhover'; break;
						case '' 						: $currentclass2 = 'myaccountlinkhover'; break;
					}
					?>
            <a href="myaccount/order-history" title="Order History" class="myaccountlink <?php echo $currentclass2?>"> Order History </a> 
			<a href="myaccount/change-password" title="Change Password" class="myaccountlink <?php echo $currentclass?>"> Change Password </a> <a href="myaccount/edit-profile" title="Edit Profile" class="myaccountlink <?php echo $currentclass1?>"> Edit Profile </a> </div>
        </div>
        <?php
			switch ( $pages[0] ) {
				case 'change-password' 			: include ("change-password.php"); break;
				case 'edit-profile' 			: include ("edit-profile.php"); break;
				case 'order-history-details' 	: include ("order-history-details.php"); break;
				default :include ("change-password.php");
				default :include ("order-history.php");
			}
			?>
      </div>
    </div>
  </div>
</div>
