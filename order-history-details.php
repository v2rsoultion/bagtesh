<?php
if( $_SESSION['loginu_id'] == "" ) {
	echo "<script>window.location='index.php'></script>";
}
global $db, $product;

$cartDetails = $db->get_results( "select * from ".TABLE_CHECKOUT." where checkout_register_user = '".$_SESSION['loginu_id']."'" );
?>
<div id="middlebg_inner">
	<div id="middle1_inner">
		<div class="regtitiel"> Order Details of #<?php echo $cartDetails[0]->checkout_id?> </div>
		
		<div class="orderdeatilpage">
			
			<p>
				Order #<?php echo $cartDetails[0]->checkout_id?> was placed on 
				<?php echo date( 'd M, Y', strtotime( $cartDetails[0]->checkout_date ) )?>
				and is currently processing.
			</p>
			
			
			<div class="customerdetails">
				<div class="regtitiel"> ORDER DETAILS </div>
				<table summary="table designs" width="40%" class="checkoutcart">
					<thead>
						<tr>
							<th scope="col"> Product </th>
							<th scope="col"> Total </th>
						</tr>
					</thead>
					
					<tbody>
						<?php
						$srn = 0;
						$classCounter = 0;
						$cartDetails1 = $db->get_results( "select * from ".TABLE_CART." where cart_checkout_id = '".$cartDetails[0]->checkout_id."'" );
						
						foreach ( $cartDetails1 as $fcartDetails ) {
							$srn++;
							$classCounter++;
							$total = 0;
							
							if ( $classCounter == 2 ) {
								$classOdd = 'class="odd"';
								$classCounter = 0;
							} else {
								$classOdd = 'class="even"';
							}
							
							$productDet = $product->products_by_id( $fcartDetails->cart_product_id );
							
							$total = $fcartDetails->cart_qty * $fcartDetails->cart_price;
						?>
							<tr <?php echo $classOdd ?>>
								<td style="border: none;">
									<?php echo $productDet[0]->product_name; ?> x <?php echo $fcartDetails->cart_qty; ?>
								</td>
								
								<td valign="middle"> $<?php echo $total ?> </td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			
			<div class="customerdetails">
				<div class="regtitiel"> CUSTOMER DETAILS </div>
				<div>
					<p> 
						<strong> Email: </strong> 
						<a href="mailto:<?php echo $cartDetails[0]->checkout_email; ?>" title="">
							<?php echo $cartDetails[0]->checkout_email; ?>
						</a>
					</p>
					<p> <strong> Telephone: </strong> <?php echo $cartDetails[0]->checkout_phone; ?> </p>
				</div>
			</div>
			
			<?php
			if( $cartDetails[0]->checkout_ship_fname != "" ) {
			?>
				<div class="customerdetails">
					<div class="regtitiel"> BILLING ADDRESS </div>
					<div>
						<p> <?php echo $cartDetails[0]->checkout_ship_fname; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_lname; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_phone; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_address; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_country; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_state; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_city; ?> </p>
						<p> <?php echo $cartDetails[0]->checkout_ship_zip; ?> </p>
					</div>
				</div>
			<?php } ?>
			
		</div>
		
	</div>
</div>