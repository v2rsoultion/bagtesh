<style>
.mrg  table {
	border-collapse: collapse;
	font: normal 11px verdana, arial, helvetica, sans-serif;
	background: #fff !important;
}
caption {
	text-align: left;
	font: normal 11px verdana, arial, helvetica, sans-serif;
	background: transparent;
}
.mrg table td, th {
	border: 1px dashed #B85A7C;
	border-top: none;
	padding: .8em;
}
.mrg table thead th, tfoot th {
	font: bold 14px verdana, arial, helvetica, sans-serif;
	border: none;
	text-align: left;
	background: #ff6600;
	color: #fff;
	padding: 10px;
}
.mrg table tbody td a {
	background: transparent;
	text-decoration: none;
	color: #000;
}
.mrg table tbody td a:hover {
	background: transparent;
	color: #CE151D;
}
.mrg table tbody th a {
	font: normal 11px verdana, arial, helvetica, sans-serif;
	background: transparent;
	text-decoration: none;
	font-weight: normal;
}
.mrg table tbody th a:hover {
	background: transparent;
	color: #FFFFFF;
}
.mrg table tbody th, tbody td {
	vertical-align: middle;
	text-align: left;
}
.mrg table tfoot td {
	border: 0px solid #38160C;
	background: #006573;
	padding-top: 6px;
}
.mrg table tbody
{
	border:1px solid #f2f2f2;
}
.odd {
	background: #fff;
	transition: all 0.3s ease-in-out;
}
.odd td {
	color: #000;
}
.even {
	background: #f2f2f2;
	color: #000;
	transition: all 0.3s ease-in-out;
}
.even td {
	color: #000 !important;
}
.even:hover {
	background: #f2f2f2;
}
.odd:hover {
	background: #fff;
}
.mrg tbody tr:hover th, tbody tr.odd:hover th {
	background: #51152A;
}
.mrg
{
margin-top:40px;
margin-bottom:20px;
}



</style>

<div id="middlebg_inner">
  <div id="middle1_inner">
  <div class="middle_part">
  <div  class="mrg">
  <?php
	$sn=0;
	$query=mysql_query("select * from ". TABLE_CHECKOUT." where checkout_register_user='".$_SESSION['loginu_id']."'");
	$tot=mysql_num_rows($query);
	if($tot>0)	{
  ?>
 <table summary="table designs" width="100%" class="checkoutcart">
				<thead>
					<tr>
						<th scope="col" width="100"> Serail No. </th>
						<th scope="col" width="100"> Order No. </th>
						<th scope="col"> Date </th>
						<th scope="col"> Status </th>
						<th scope="col">Amount</th>
						
						<th scope="col"> </th>
						
					</tr>
				</thead>
				<tbody>
				<?php
				$sn=0;
				$start=0;
				$limit=50;
				$start = (isset($_GET['pn'])) ? ($_GET['pn'] - 1) * $limit : 0;
				$query=mysql_query("select * from ". TABLE_CHECKOUT." where checkout_register_user='".$_SESSION['loginu_id']."'  LIMIT $start,$limit");
				while($fetch=mysql_fetch_array($query))
				{
				$sn++;
				if($sn%2==0)
				{
				$sel='"class="even"' ;
				}
				else
				{
				$sel='"class="odd"' ;
				}
				?>
					<tr <?php echo $sel;  ?> >

							<td style="border: none;"> <?php echo  $sn; ?> </td>

							<td valign="middle"><?php echo $fetch['ordernumber']  ?></td>
							<td valign="middle"><?php echo $fetch['checkout_date'] ?></td>
							<td valign="middle">	<?php echo $fetch['orderstatus'] ?></td>
							<td valign="middle">	<?php echo $fetch['tot_amt'] ?>	</td>
				
			
				<td valign="middle">		
						<a href="order_historydetail?id=<?php echo $fetch['checkout_id']?>">View Detail</a></STRONG></td>

						</tr>
						<?php 
						}
						?>
						
			</tbody>

			</table>
			<?php }
			else{	?>
			<div class="mes">No Data in Order History</div>
			<?php 	} ?>
			
			<div style="height:40px;">
			<div style="clear:both"></div>
							<?php
								$tot_rec = mysql_num_rows(mysql_query("select * from ". TABLE_CHECKOUT." where checkout_register_user='".$_SESSION['loginu_id']."'"));
								$tot_page = ceil($tot_rec / $limit);
								$prev = $_GET['pn'] - 1;
								$next = isset($_GET['pn']) ? $_GET['pn'] + 1 : 2;
								if($tot_page > 1) {
							?>
					<div class="pagination_class">
						<ul>
							<?php
							if($prev >= 1) {
							?>
							<li><a href="<?php echo $SITEPATH2?>myaccount/order-history">First</a></li>
							<li><a href="<?php echo $SITEPATH2?>myaccount/order-history/?pn=<?php echo $prev ?>">&laquo; Previous</a></li>
							<?php
								}
								$start_point = 1;
								$end_point = $tot_page;
								if($tot_page > 9) {
								if($_GET['pn'] < 9) {
									$end_point = 9;
								} else {
									$end_point  = ($_GET['pn'] + 4) < $tot_page ? $_GET['pn'] + 4 : $tot_page;
									$start_point = $end_point - 8;
								}
							}
						for($pageno = $start_point; $pageno <= $end_point; $pageno++) {
							$pagn = ($pageno == $_GET['pn']) ? "" : ' href="'.$SITEPATH2.'myaccount/order-history/?pn='.$pageno.'"';
						?>
							<li><a<?php echo $pagn ?>><?php echo $pageno ?></a></li>
							<?php
						}
						if($next <= $tot_page) {
						?>
							<li><a href="<?php echo $SITEPATH2?>myaccount/order-history/?pn=<?php echo $next ?>">Next &raquo;</a></li>
							<li><a href="<?php echo $SITEPATH2?>myaccount/order-history/?pn=<?php echo $tot_page ?>">Last</a></li>
							<?php
							}
							?>
						</ul>
					</div>
					<?php } ?>
			</div>
</div>
			

		</div>

	</div>
  </div>
</div>
