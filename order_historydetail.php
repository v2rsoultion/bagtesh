<style>
.mrg  table {
	border-collapse: collapse;
	font: normal 11px verdana, arial, helvetica, sans-serif;
	background: #fff !important;
}
caption {
	text-align: left;
	font: normal 11px verdana, arial, helvetica, sans-serif;
	background: transparent;
}
.mrg table td, th {
	border: 1px dashed #B85A7C;
	border-top: none;
	padding: .8em;
}
.mrg table thead th, tfoot th {
	font: bold 14px verdana, arial, helvetica, sans-serif;
	border: none;
	text-align: left;
	background: #ff6600;
	color: #fff;
	padding: 10px;
}
.mrg table tbody td a {
	background: transparent;
	text-decoration: none;
	color: #000;
}
.mrg table tbody td a:hover {
	background: transparent;
	color: #CE151D;
}
.mrg table tbody th a {
	font: normal 11px verdana, arial, helvetica, sans-serif;
	background: transparent;
	text-decoration: none;
	font-weight: normal;
}
.mrg table tbody th a:hover {
	background: transparent;
	color: #FFFFFF;
}
.mrg table tbody th, tbody td {
	vertical-align: middle;
	text-align: left;
}
.mrg table tfoot td {
	border: 0px solid #38160C;
	background: #006573;
	padding-top: 6px;
}
.mrg table tbody
{
	border:1px solid #f2f2f2;
}
.odd {
	background: #fff;
	transition: all 0.3s ease-in-out;
}
.odd td {
	color: #000;
}
.even {
	background: #f2f2f2;
	color: #000;
	transition: all 0.3s ease-in-out;
}
.even td {
	color: #000 !important;
}
.even:hover {
	background: #f2f2f2;
}
.odd:hover {
	background: #fff;
}
.mrg tbody tr:hover th, tbody tr.odd:hover th {
	background: #51152A;
}
.mrg
{
margin-top:40px;
margin-bottom:20px;
}



</style>
<div id="middlebg_inner">
  <div id="middle1_inner">
   <div class="middle_part">
     <div class="breadcump"><a href="<?php $SITEPATH2 ?>">Home</a> &raquo;Order History Detail </div>
      <h1>Order History Detail</h1>
   <div  class="mrg">
   <div class="bcol" ><a href="<?php echo $SITEPATH2 ?>myaccount" title="My Account">Back</a></div>
 <table summary="table designs" width="70%" class="checkoutcart">
				<thead>
					<tr>
						<th scope="col" width="100">No. </th>
						<th scope="col"> Product Name</th>
						<th scope="col"> Product Code</th>
						<th scope="col"> Price </th>

					</tr>
				</thead>
				<tbody>
				<?php
			$fet=mysql_query("SELECT * FROM `cart` where cart_checkout_id='".$_GET['id']."'");
				while($query=mysql_fetch_array($fet)) 
				{	
				$quer=mysql_query("select * from ". TABLE_PRODUCT." where  product_id='".$query['cart_product_id']."'");
				while($fetchin=mysql_fetch_array($quer))
				{
				$sn++;
				if($sn%2==0)
				{
				$sel='"class="even"' ;
				}
				else
				{
				$sel='"class="odd"' ;
				}
				?>
					<tr <?php echo $sel;  ?> >

							<td style="border: none;"> <?php echo  $sn; ?> </td>
							<td valign="middle"><?php echo $fetchin['product_name'] ."&nbsp;*&nbsp;". $query['cart_qty']?></td>
							<td valign="middle"><?php echo $fetchin['product_code'] ?></td>
							
							<?php if($fetchin['product_dis_price']!="" &&  $fetchin['product_dis_price']!=0)
							{ ?>
							<td valign="middle">$<?php echo $fetchin['product_dis_price'] * $query['cart_qty']?></td>
							<?php }else{ ?>
							<td valign="middle">$<?php echo $fetchin['product_price'] * $query['cart_qty'] ?></td>
							<?php } ?>
						</tr>

					
						<?php 
						}}
						?>
						
			</tbody>

			</table>
</div>

</div>
</div>
</div>